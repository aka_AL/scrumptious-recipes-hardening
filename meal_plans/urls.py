from django.urls import path
from .views import (
    MealPlansListView,
    MealPlansCreateView,
    MealPlansDetailView,
    MealPlansUpdateView,
    MealPlansDeleteView,
)

urlpatterns = [
    path("", MealPlansListView.as_view(), name="meal_plans_list"),
    path("new/", MealPlansCreateView.as_view(), name="meal_plan_new"),
    path("<int:pk>/", MealPlansDetailView.as_view(), name="meal_plan_details"),
    path(
        "<int:pk>/edit/", MealPlansUpdateView.as_view(), name="meal_plan_edit"
    ),
    path(
        "<int:pk>/delete/",
        MealPlansDeleteView.as_view(),
        name="meal_plan_delete",
    ),
]
