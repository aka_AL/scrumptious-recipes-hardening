from django.shortcuts import render
from django.urls import reverse_lazy
from meal_plans.models import MealPlan
from django.views.generic import (
    ListView,
    CreateView,
    DetailView,
    UpdateView,
    DeleteView,
)
from django.contrib.auth.mixins import LoginRequiredMixin
from django.shortcuts import redirect

# Create your views here.
class MealPlansListView(ListView):
    model = MealPlan
    context_object_name = "meal_plan_list"
    template_name = "meal_plans/list.html"
    paginate_by = 2

    def get_queryset(self):
        return MealPlan.objects.filter(owner=self.request.user)


class MealPlansCreateView(LoginRequiredMixin, CreateView):
    model = MealPlan
    template_name = "meal_plans/new.html"
    fields = ["name", "recipes", "date"]

    def form_valid(self, form):
        form.instance.author = self.request.user
        return super().form_valid(form)

    def form_valid(self, form):
        plan = form.save(commit=False)
        plan.owner = self.request.user
        plan.save()
        form.save_m2m()
        return redirect("meal_plan_details", pk=plan.id)


class MealPlansDetailView(DetailView):
    model = MealPlan
    context_object_name = "meal_plan_details"
    template_name = "meal_plans/detail.html"


class MealPlansUpdateView(LoginRequiredMixin, UpdateView):
    model = MealPlan
    template_name = "meal_plans/edit.html"
    fields = ["name", "recipes", "date"]

    def get_success_url(self):
        return reverse_lazy("meal_plan_details", args=[self.object.id])

    def form_valid(self, form):
        form.instance.author = self.request.user
        return super().form_valid(form)


class MealPlansDeleteView(LoginRequiredMixin, DeleteView):
    model = MealPlan
    template_name = "meal_plans/delete.html"
    success_url = reverse_lazy("meal_plans_list")
