from django import template

register = template.Library()


def resize_to(ingredient, user_servings):
    servings = int(ingredient.recipe.servings)
    print(servings)
    if servings is not None and user_servings is not None:
        try:
            ratio = user_servings / servings
            return ratio * ingredient.amount
        except:
            pass
    else:
        return ingredient.amount


register.filter(resize_to)
